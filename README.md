# zkUnityOSC

Extensions to 
UnityOSC - Open Sound Control interface for the Unity3d game engine      Copyright (c) 2012 Jorge Garcia Martin

To provide additional and simplified access to messaging.



Note:  zkUnityOSC  depends on a included and modified version of UnityOSC/OSCHandler, which has  two additional methods:   Awake(), and RemoveClient()

Both methods are annotated with the following comment:
// added by Zack


A usage example, complete with testing patch (for use with pure-data) can be found in:
zkunityosc/Assets/zUnityOSC/example






