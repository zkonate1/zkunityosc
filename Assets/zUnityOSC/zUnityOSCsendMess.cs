﻿using UnityEngine;
using UnityOSC;
using System;
using System.Net;
using System.Collections.Generic;

public class zUnityOSCsendMess : MonoBehaviour
{

    public string streamName = "default";
    public string oscAddress = "";
 

    private List<object> outputList = new List<object>();
    public static List<string> streamNames = new List<string>();

    private zUnityOSCtx osctx;



    private void Awake()
    {
    
        string userID = SystemInfo.deviceName;

        if (oscAddress == "") oscAddress = "/" + userID + "/6dof";
        if (streamName == "") streamName = "default";
    }


    void Start()
    {
        osctx = zUnityOSCtx.getInstance(streamName);

        if (osctx == null)
        {
            if (streamName == "default")
            {
                GameObject gob = new GameObject("zUnityOSCtx.autoGen.DefaultStream");
                gob.AddComponent<zUnityOSCtx>();
                osctx = gob.GetComponent<zUnityOSCtx>();   // creates stream with defaults, as defined in the zUnityOSCtx class
                osctx.streamName = streamName;
            }
            else
            {
                Debug.LogError(name + " " + GetType() + " zUnityOSCtx instance for stream: " + streamName + " undefined, aborting");
                Destroy(this);
            }
        }


        if (!streamNames.Contains(streamName))
        {
            string message = String.Format(": Creating TX stream: {2} to ip: {0} port: {1}", osctx._ipAddress, osctx.port, streamName);
            OSCHandler.Instance.CreateClient(streamName, osctx._ipAddress, osctx.port);
            Debug.Log(GetType() + message);
            streamNames.Add(streamName);
        }

        outputList = new List<object>();
    }

    private void OnDestroy()
    {
        // OSCHandler.Instance.RemoveClient(streamName);
    }

    private void OnApplicationQuit()
    {

    }

    public void send()
    {
        if (osctx.oscMute) return;

        outputList.Clear();

        OSCHandler.Instance.SendMessageToClient(streamName, oscAddress, outputList);
    }

    public void sendI(int val1)
    {
        if (osctx.oscMute) return;
        outputList.Clear();

        outputList.Add(val1);

        OSCHandler.Instance.SendMessageToClient(streamName, oscAddress, outputList);
    }

    public void sendS(string val1)
    {
        if (osctx.oscMute) return;

        outputList.Clear(); 

        outputList.Add(val1);

        OSCHandler.Instance.SendMessageToClient(streamName, oscAddress, outputList);
    }


    public void sendF(float val1)
    {
        if (osctx.oscMute) return;

        outputList.Clear(); 

        outputList.Add(val1);

        OSCHandler.Instance.SendMessageToClient(streamName, oscAddress, outputList);
    }

    public void sendSI(string val1, int val2)
    {
        if (osctx.oscMute) return;
        outputList.Clear();

        outputList.Add(val1);
        outputList.Add(val2);

        OSCHandler.Instance.SendMessageToClient(streamName, oscAddress, outputList);
    }


}

