﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityOSC;

public class zUnityOSCrotateGameObj : MonoBehaviour
{

    public string streamName = "default";
    private zUnityOSCrx oscrx;
    public bool debug = false;
    private bool oscListening = false;


    public enum quatOrder { XYZW, YXZW }
    [Tooltip("this message expects three values X Y Z, two additional messages are recognized:   yourMessage/y and yourMessage/xz, that expect one and two args respectively")]


    public string OSCaddress;



    private Transform transformToMove;

    //movementModeProp = serializedObject.FindProperty ("movementMode");

    public bool constrainX = false;

    public bool constrainY = false;

    public bool constrainZ = false;

    public Vector3 constaintsXYZ = new Vector3();



    //public float smoothRotMs = 30f;
    //public float smoothQuatMs = 30f;


    //Whether we are currently interpolating or not
    private bool _isLerping;

    //The start and finish positions for the interpolation
    private Quaternion _startRotation;
    private Quaternion _endRotation;

    //The Time.time value when we started the interpolation
    private float _timeStartedLerping;

    private bool _start = false;

    [Header("")]


    public float rotationSmoothingTime = 0.25f;

    [Header("OSCrx Polling Option")]

    // OSC POLLING CONTROL
    private Vector4 _oscCurValue;
    private Vector4 _oscLastValue;
    public bool OSCpollingEnable = true;
    public float OSCpollIntervalMs = 30f;
    // polling interval ms for current OSC values
    private float _lastPollTime;


    [Header("")]
    [Tooltip("use YXZW for use with Gyrosc")]
    public quatOrder quatAxisOrder = quatOrder.XYZW;

    private void OnValidate()
    {
        if (!_start)
            return;
        if (rotationSmoothingTime <= 0)
            rotationSmoothingTime = 0.0001f;
        if (OSCpollIntervalMs < 0)
            OSCpollIntervalMs = 0f;
    }


    void Start()
    {
        _oscCurValue = new Vector4(0, 0, 0, 0);
        _oscLastValue = new Vector4(0, 0, 0, 0);

        if (OSCaddress.Length == 0)
        {
            OSCaddress = "/" + transform.name + "/rotation";  // automatically create an OSC address using the object's name, making:  "/objName/position"
        }

        oscrx = zUnityOSCrx.getInstance(streamName);

        if (oscrx == null)
        {
            if (streamName == "default")
            {
                if (debug) Debug.Log(name + " " + GetType() + ".Start(): zUnityOSCrx instance for stream: " + streamName + " undefined, instantiating the default instance");
                GameObject gob = new GameObject("zUnityOSCrx.autoGen.DefaultStream");
                gob.AddComponent<zUnityOSCrx>();
                oscrx = gob.GetComponent<zUnityOSCrx>();   // creates stream with defaults, as defined in the zUnityOSCstreamInfo class
                oscrx.streamName = streamName;
            }
            else
            {
                Debug.LogError(name + " " + GetType() + " zUnityOSCrx instance for stream: " + streamName + " undefined, aborting");
                Destroy(this);
            }
        }

        _start = true;
        oscListen(true);
        OnEnable();   // this can not be called until START has set up the state
    }

    void oscListen(bool state)
    {
        if (oscrx == null) return;

        if (state)
        {
            if (oscListening != true)
            {
                // subscribe
                oscrx.OnOscMessage += OnOscMessage;
                oscListening = true;
            }
        }
        else
        {
            if (oscListening == true)
            {
                oscrx.OnOscMessage -= OnOscMessage;
                oscListening = false;
            }
        }
    }


    public void OnEnable()
    {

        if (!_start)
            return;

        oscListen(true);


        if (transformToMove == null)
        {
            Transform hostTransform = GetComponent<Transform>();
            if (hostTransform != null)
                transformToMove = hostTransform;
        }
        //Debug.Log("ONENABLE  OBJ NAME: " + transformToMove.name);
        _startRotation = _endRotation = transformToMove.localRotation;

    }

    void OnDisable()
    {
        if (!_start)
            return;
        oscListen(false);
    }


    //We do the actual interpolation in FixedUpdate(), since we're dealing with a rigidbody
    void Update()
    {

        if (OSCpollingEnable)
        {
            if (1000 * (Time.unscaledTime - _lastPollTime) > OSCpollIntervalMs)
            {
                _lastPollTime = Time.unscaledTime;
                processQuat();
            }
        }

        if (_isLerping)
        {
            //We want percentage = 0.0 when Time.time = _timeStartedLerping
            //and percentage = 1.0 when Time.time = _timeStartedLerping + timeTakenDuringLerp
            //In other words, we want to know what percentage of "timeTakenDuringLerp" the value
            //"Time.time - _timeStartedLerping" is.
            float timeSinceStarted = Time.time - _timeStartedLerping;
            float percentageComplete = timeSinceStarted / rotationSmoothingTime;

            //Perform the actual lerping.  Notice that the first two parameters will always be the same
            //throughout a single lerp-processs (ie. they won't change until we hit the space-bar again
            //to start another lerp)
            transformToMove.localRotation = Quaternion.Slerp(_startRotation, _endRotation, percentageComplete);

            //When we've completed the lerp, we set _isLerping to false
            if (percentageComplete >= 1.0f)
            {
                _isLerping = false;
            }
        }
    }

    void OnOscMessage(OSCMessage message)
    {
        float x = 0;
        float y = 0;
        float z = 0;
        float w = 0;

        if (message.Address != OSCaddress) return;

        if (message.Data.Count < 3)
        {
            Debug.LogWarning(transform.name + " : " + GetType() + " : " + "OnOscMessage(): 3 or 4 values required: " + OSCaddress + " X Y Z (W), ignoring message");
            return;
        }

        if (message.Data[0] is float)
            x = (float)message.Data[0];

        if (message.Data[1] is float)
            y = (float)message.Data[1];

        if (message.Data[2] is float)
            z = (float)message.Data[2];

        if (message.Data.Count == 3)
        {
            Quaternion q = Quaternion.Euler(x, y, z);
            _oscCurValue.Set(q.x, q.y, q.z, q.w);

        }
        else    // its a quat
        {

            if (message.Data[3] is float)
                w = (float)message.Data[3];

            // store current value

            switch (quatAxisOrder)
            {
                case quatOrder.YXZW:
                    _oscCurValue.Set(y, x, z, w);
                    break;
                case quatOrder.XYZW:
                default:
                    _oscCurValue.Set(x, y, z, w);
                    break;
            }
        }

        if (!OSCpollingEnable)
            processQuat();
    }

    void processQuat()
    {
        float data0, data1, data2, data3;

        if (_oscCurValue == _oscLastValue)
            return;

        data0 = _oscCurValue.x;
        data1 = _oscCurValue.y;
        data2 = _oscCurValue.z;
        data3 = _oscCurValue.w;
        _oscLastValue = _oscCurValue;

        if (transformToMove == null)
            return;


        float x, y, z, w;
        Quaternion targetRot;

        x = data0;
        y = data1;
        z = data2;
        w = data3;

        targetRot = new Quaternion(x, y, z, w);

        // if true, oh shit, we'll have to constrain rotation and accept the consequences
        if (constrainX || constrainY || constrainZ)
        {
            float pitch, yaw, roll;
            Vector3 eulers = targetRot.eulerAngles;

            pitch = (constrainX) ? constaintsXYZ.x : eulers.x;
            yaw = (constrainY) ? constaintsXYZ.y : eulers.y;
            roll = (constrainZ) ? constaintsXYZ.z : eulers.z;

            targetRot = Quaternion.Euler(pitch, yaw, roll);

        }
        _isLerping = true;
        _timeStartedLerping = Time.time;

        //We set the start position to the current position, and the finish to 10 spaces in the 'forward' direction
        _startRotation = transformToMove.localRotation;
        _endRotation = targetRot;
    }
}

