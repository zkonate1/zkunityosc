﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityOSC;

public class zUnityOSCapply : MonoBehaviour
{

    public string streamName = "default";
    private zUnityOSCrx oscrx;

    private bool oscListening = false;


    private string _oscMatchAddr = "";
    private bool _start = false;


    private bool wildCardMode = false;

    public string OSCaddress = "";      // automatically creates an OSC address using the object's name, making:  "/objName/position"
    public bool alsoSendToChildren = false;
    public bool debug = false;
    //[Tooltip("make sure that the OscIn objects in the environment have a corresponding tag")]


    private void Awake()
    {
        if (streamName == "") streamName = "default";
    }


    void Start()
    {
        // OSC RX STUFF
        oscrx = zUnityOSCrx.getInstance(streamName);

        if (oscrx == null)
        {
            if (streamName == "default")
            {
                if (debug) Debug.Log(name + " " + GetType() + ".Start(): zUnityOSCrx instance for stream: " + streamName + " undefined, instantiating the default instance");
                GameObject gob = new GameObject("zUnityOSCrx.autoGen.DefaultStream");
                gob.AddComponent<zUnityOSCrx>();
                oscrx = gob.GetComponent<zUnityOSCrx>();   // creates stream with defaults, as defined in the zUnityOSCstreamInfo class
                oscrx.streamName = streamName;
            }
            else
            {
                Debug.LogError(name + " " + GetType() + " zUnityOSCrx instance for stream: " + streamName + " undefined, aborting");
                Destroy(this);
            }
        }
        oscListen(true);


        //  parsing stuff
        if (OSCaddress.Length == 0)
        {
            Debug.LogWarning(transform.name + " : " + GetType() + " : " + "Start(): no OSC address provided, using nodeName");
            OSCaddress = "/" + name + "/*";
        }
        _oscMatchAddr = OSCaddress = OSCaddress.Replace(" ", System.String.Empty);  // flush out any white spaces
        checkOSCprependedPath();
        _start = true;
    }


    // Update is called once per frame
    void Update()
    {
        //if (reciever.hasWaitingMessages())
        //{
        //    OSCMessage msg = reciever.getNextMessage();
        //    if (debug) Debug.Log(name+" "+GetType()+ ".Update(): port:"+ port + string.Format("message received: {0} {1}", msg.Address, DataToString(msg.Data)));
        //    OnOSCMessage(msg);
        //}
    }


    void OnValidate()
    {
        if (!_start)
            return;
        if (OSCaddress != _oscMatchAddr)
            checkOSCprependedPath();
    }

    void oscListen(bool state)
    {
        if (oscrx == null) return;

        if (state)
        {
            if (oscListening != true)
            {
                // subscribe
                oscrx.OnOscMessage += OnOSCMessage;
                oscListening = true;
            }
        }
        else
        {
            if (oscListening == true)
            {
                oscrx.OnOscMessage -= OnOSCMessage;
                oscListening = false;
            }
        }
    }


    public void OnEnable()
    {
        oscListen(true);
    }


    void OnDisable()
    {
        oscListen(false);

        //// Unsubscribe from messsages
        //if (oscIn != null)
        //oscIn.onAnyMessage.RemoveListener(OnOSCMessage);   // unsubsccribe
    }



    void checkOSCprependedPath()
    {
        if (!OSCaddress.Contains("/") || !OSCaddress.StartsWith("/"))
        {
            Debug.LogError(string.Format("{0}.Awake():  OSC path format error: {1}, aborting", GetType(), OSCaddress), transform);
            _oscMatchAddr = "";
            return;
        }

        _oscMatchAddr = OSCaddress = OSCaddress.Replace(" ", System.String.Empty);  // flush out any white spaces

        wildCardMode = false;

        if (OSCaddress.EndsWith("/*"))    // all good
        {
            wildCardMode = true;
            _oscMatchAddr = OSCaddress.TrimEnd(new char[] { '/', '*' });
        }
        else if (OSCaddress.EndsWith("/"))
        {
            _oscMatchAddr = OSCaddress.TrimEnd(new char[] { '/' });
            OSCaddress = _oscMatchAddr;
            //OSCaddress += "*";
        }
        else
        {
            _oscMatchAddr = OSCaddress;
        }
    }

    public void OnOSCMessage(OSCMessage msg)
    {
        string address = msg.Address;
        int matchLen = _oscMatchAddr.Length;

        string hookName = "nothing";
        string sval;
        float fval;
        int ival;

        if (debug)
        {
            Debug.Log(transform.name + ": " + GetType() + "OnOSCMessage()  MESS: " + address + "   arg count: " + msg.Data.Count);
        }

        string[] keys = address.Split('/');
        //            foreach (string word in keys)
        //            {
        //                // Debug.Log("ADDRESS ITEM: " + word);
        //            }

        if (keys.Length < 2)  // meaning, if it's just  "/" ,  bail
        {
            Debug.LogWarning(transform.name + ": " + GetType() + ".OnOSCMessage():  ignoring empty address");
            return;
        }

        if (address.Length < matchLen) // message address can not match..  too short
        {
            if (debug)
            {
                Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): message address not matched, ignoring message: " + address);
            }
            return;
        }

        //Debug.Log("_oscMatchAddr : " + _oscMatchAddr + " matchingAddress: " + address.Substring(0, matchLen));
        //Debug.Log("_oscMatchAddr.Len : " + _oscMatchAddr.Length + " matchingAddress: " + address.Substring(0, matchLen).Length);


        if (_oscMatchAddr == address.Substring(0, matchLen))  // YES,  the message contains at least the match 
        {

            if (debug)
                Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): :address match: " + address.Substring(0, matchLen));


            if (wildCardMode)  // we are expecting additional keys in the received message
            {

                if (_oscMatchAddr.Length == address.Length)  // no extra keys in the received address,  no good for wildcard mode
                {
                    Debug.LogWarning(transform.name + ": " + GetType() + ".OnOSCMessage(): address lacking additional wild card key, ignoring message: " + address);
                    return;
                }

                // else chack the rest of the received address for more keys
                hookName = address.Substring(matchLen + 1);
                keys = hookName.Split('/');

                //Debug.Log("KEYS LENGTH= " + keys.Length);
                foreach (string word in keys)
                {
                    //Debug.Log("KEY ITEM: " + word);
                }

                if (keys.Length == 1 && keys[0].Length != 0)
                {
                    hookName = keys[0];

                    //Debug.Log("\t\t hookName: " + hookName + "  hookName Length = :" + hookName.Length);
                }
                else
                {
                    Debug.LogWarning(transform.name + ": " + GetType() + ".OnOSCMessage(): wild portion of address must contain ONE key, ignoring message: " + address);
                    return;
                }
            }
            else  // not looking for wild card, use last key in message
            {
                if (_oscMatchAddr.Length == address.Length)
                {
                    keys = address.Split('/');
                    hookName = keys[keys.Length - 1];
                    if (debug) Debug.Log("\t\t hookName: " + hookName + "  hookName Length = :" + hookName.Length);
                }
                else
                {
                    if (debug)
                    {
                        Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): received address: " + address + " does not match: " + _oscMatchAddr);
                    }

                    return; // message does not correspond to the saught message
                }


                //Debug.Log("\t\t hookName: " + hookName);
            }
        }
        else  // message not matched
        {
            if (debug) Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): received address: " + address + " does not match: " + _oscMatchAddr);
            return;
        }


        // address is good but more than one data in message... bail
        if (msg.Data.Count > 1)
        {
            Debug.LogWarning(transform.name + ": " + GetType() + ".OnOSCMessage():  can only have maximum of one datum in message, aborting ");
            return;
        }


        if (debug) Debug.Log("\t\tMESSAGE MATCHED: " + _oscMatchAddr + " for method: " + hookName);
        // send message to all scripts on this gameobject, and its children
        // gameObject.BroadcastMessage(hookName, (float)msg.Data[0], SendMessageOptions.DontRequireReceiver);  


        // OSC address only, no data to include in message
        if (msg.Data.Count == 0)
        {

            gameObject.SendMessage(hookName, null, SendMessageOptions.DontRequireReceiver);
            if (alsoSendToChildren)
            {
                Transform[] transforms = gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform t in transforms)
                {
                    t.SendMessage(hookName, null, SendMessageOptions.DontRequireReceiver);
                }
            }

            return;
        }



        sval = msg.Data[0].ToString();


        bool isNumericInt = int.TryParse(sval, out ival);
        bool isNumericFloat = float.TryParse(sval, out fval);

        if (debug)
        {
            if (isNumericInt)
                Debug.Log(name + " " + GetType() + " DataToString:  Value: " + ival + " is an int");
            else if (isNumericFloat)
                Debug.Log(name + " " + GetType() + " DataToString:  Value: " + fval + " is a float");
            else
                Debug.Log(name + " " + GetType() + " DataToString:  Value: " + sval + "is not a number");

        }

        // msg.Data[0] is an int, cast as float
        if (isNumericInt)
        {
            fval = (float)ival;
            isNumericFloat = true;
        }

        // msg.Data[0] is a float
        if (isNumericFloat)
        {
            gameObject.SendMessage(hookName, fval, SendMessageOptions.DontRequireReceiver);

            if (alsoSendToChildren)
            {
                Transform[] transforms = gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform t in transforms)
                {
                    t.SendMessage(hookName, fval, SendMessageOptions.DontRequireReceiver);
                }
            }
            return;
        }

        // msg.Data[0]  must be is a string
        gameObject.SendMessage(hookName, sval, SendMessageOptions.DontRequireReceiver);

        if (alsoSendToChildren)
        {
            Transform[] transforms = gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform t in transforms)
            {
                t.SendMessage(hookName, sval, SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    private string DataToString(List<object> data)
    {
        string buffer = "";

        for (int i = 0; i < data.Count; i++)
        {
            buffer += data[i].ToString() + " ";
        }

        buffer += "\n";

        return buffer;
    }

}
