﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityOSC;
using System.Net;
using System.Net.NetworkInformation;

public class zUnityOSCtx : MonoBehaviour
{

    //public static bool oscTXstate = false; // class variable for OSCtx -  off by default   --- NOT USED
    public bool oscMute = false;

    public static List<zUnityOSCtx> instances = new List<zUnityOSCtx>();

    // defaults 
    public string streamName = "default";
    public int port = 9000;

    [Tooltip("reserved addresses:  localhost and broadcast (which will broadcast on the local network)")]
    public string ipAddress = "localhost";
    [HideInInspector]
    public IPAddress _ipAddress;

    public float txThresholdIndex = 1f;  // to be refined

    public static float TXtresholdIndex = 1f;  // to be refined

    public bool debug;


    //public void setOSCtx(bool state)
    //{
    //    oscTXstate = state;
    //}


    public static zUnityOSCtx getInstance(string sName)
    {
        zUnityOSCtx defaultInstance = null;


        foreach (zUnityOSCtx instance in instances)
        {
            if (sName == instance.streamName)
            {
                return (instance);
            }
            if (instance.streamName == "default")
                defaultInstance = instance;
        }
        // otherwise no match was found
        // play it safe and instantiate a default instance if none exist
        //if (defaultInstance == null) 
        //defaultInstance = new zUnityOSCstreamInfo();

        if (defaultInstance == null)
            Debug.LogWarning("zUnityOSCtx.getInstance(}:  no stream instance found for stream :" + sName);


        return defaultInstance;
    }


    void Awake()
    {
        oscMute = false;

        if (ipAddress == "") ipAddress = "default";
        switch (ipAddress)
        {
            case "localhost":
                _ipAddress = IPAddress.Loopback;
                break;
            case "broadcast":
                string hostName = System.Net.Dns.GetHostName();
                //string myIP = System.Net.Dns.GetHostEntry(hostName).AddressList[0].ToString();

                // string myIP  = Network.player.ipAddress; 
                string myIP = LocalIPAddress();
                int lastDotPos = myIP.LastIndexOf(".");
                ipAddress = myIP.Substring(0, lastDotPos + 1) + "255";
                _ipAddress = IPAddress.Parse(ipAddress);
                break;
            default:
                _ipAddress = IPAddress.Parse(ipAddress);
                break;

        }

        if (OSCHandler.Instance == null)
        {
            if (debug) Debug.Log(name + "." + GetType() + " Awake(): provoke OSCHandler singleton instantation, if not defined");
        }
        //oscTXstate = oscTXstateOption;

    }





    // Use this for initialization
    void Start()
    {

    }

    void OnEnable()
    {
        instances.Add(this);
    }

    void OnDisable()
    {
        instances.Remove(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (TXtresholdIndex != txThresholdIndex)
            TXtresholdIndex = txThresholdIndex;
    }

    string LocalIPAddress()
    {
        string ipaddr = zIPManager.myIP();
        if (debug) Debug.Log(name + "." + GetType() + " LocalIPAddress(): ASSIGNING IP: " + ipaddr);
        return ipaddr;
    }

}
