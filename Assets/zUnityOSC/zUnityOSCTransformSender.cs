using UnityEngine;
using UnityOSC;
using System;
using System.Net;
using System.Collections.Generic;

public class zUnityOSCTransformSender : MonoBehaviour
{

    private static int autoGenCount = 0;
    public static bool oscInitState = false;
    public string streamName = "default";
    public string oscAddress = "";

    public GameObject trackedGameObject;

    private Vector3 _currentPosition = Vector3.zero;
    private Vector3 _currentRotationEuler = Vector3.zero;
    private Quaternion _currentRotation;

    private float _lastSum = 0f;

    private List<object> outputList = new List<object>();
    private string myOSCid;
    public static List<string> streamNames = new List<string>();

    private zUnityOSCtx osctx;



    private void Awake()
    {


        if (trackedGameObject == null) trackedGameObject = gameObject;

        string userID = SystemInfo.deviceName;

        if (oscAddress == "") oscAddress = "/" + userID + "/6dof";
        if (streamName == "") streamName = "default";
    }


    void Start()
    {
        osctx = zUnityOSCtx.getInstance(streamName);

        if (osctx == null)
        {
            if (streamName == "default")
            {
                GameObject gob = new GameObject("zUnityOSCtx.autoGen.DefaultStream");
                gob.AddComponent<zUnityOSCtx>();
                osctx = gob.GetComponent<zUnityOSCtx>();   // creates stream with defaults, as defined in the zUnityOSCtx class
                osctx.streamName = streamName;
            }
            else
            {
                Debug.LogError(name + " " + GetType() + " zUnityOSCtx instance for stream: " + streamName + " undefined, aborting");
                Destroy(this);
            }
        }


        if (!streamNames.Contains(streamName))
        {
            string message = String.Format(": Creating TX stream: {2} to ip: {0} port: {1}", osctx._ipAddress, osctx.port, streamName);
            OSCHandler.Instance.CreateClient(streamName, osctx._ipAddress, osctx.port);
            Debug.Log(GetType() + message);
            streamNames.Add(streamName);
        }

        outputList = new List<object>();

        _currentPosition = trackedGameObject.transform.position;
        _currentRotation = trackedGameObject.transform.rotation;

    }

    private void OnDestroy()
    {
       // OSCHandler.Instance.RemoveClient(streamName);
    }

    private void OnApplicationQuit()
    {

    }

    void FixedUpdate()
    {
        _Update();
    }

    protected void _Update()
    {
        if (trackedGameObject == null) return;

        if (osctx.oscMute) return;

        // need to do this properly.
        //float currentSum = _currentPosition.x + _currentPosition.y + _currentPosition.z + _currentRotation.x + _currentRotation.y + _currentRotation.z + _currentRotation.w;
        // if (Mathf.Abs(currentSum - _lastSum) < zUnityOSCtx.TXtresholdIndex ) return;
        //_lastSum = currentSum;

        if (trackedGameObject.transform.position == _currentPosition && trackedGameObject.transform.rotation == _currentRotation) return;

        _currentPosition = trackedGameObject.transform.position;
        _currentRotation = trackedGameObject.transform.rotation;

        outputList.Add(_currentPosition.x);
        outputList.Add(_currentPosition.y);
        outputList.Add(_currentPosition.z);
        outputList.Add(_currentRotation.x);
        outputList.Add(_currentRotation.y);
        outputList.Add(_currentRotation.z);
        outputList.Add(_currentRotation.w);

        OSCHandler.Instance.SendMessageToClient(streamName, oscAddress, outputList);

        outputList.Clear();

    }
}

