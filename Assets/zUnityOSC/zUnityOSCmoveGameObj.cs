﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityOSC;

public class zUnityOSCmoveGameObj : MonoBehaviour
{

    public string streamName = "default";
    private zUnityOSCrx oscrx;
    public bool debug = false;
    private bool oscListening = false;


    [Tooltip("this message expects three values X Y Z, two additional messages are recognized:   yourMessage/y and yourMessage/xz, that expect one and two args respectively")]

    public string OSCaddress;

    public enum PosMapping { direct, offset }
    private Transform transformToMove;

    //movementModeProp = serializedObject.FindProperty ("movementMode");

    private Vector3 targetPos = new Vector3();
    //      private Vector3 startPos = new Vector3(); // Never used warning




    [Header("")]
    public bool Xenable = true;
    public float xscale = 1;
    public PosMapping Xmapping = PosMapping.direct;
    private float _xpos;

    [Header("")]
    public bool Yenable = true;
    public float yscale = 1;
    public PosMapping Ymapping = PosMapping.direct;
    private float _ypos;

    [Header("")]
    public bool Zenable = true;
    public float zscale = 1;
    public PosMapping Zmapping = PosMapping.direct;
    private float _zpos;



    //Whether we are currently interpolating or not
    private bool _isLerping;

    //The start and finish positions for the interpolation
    private Vector3 _startPosition;
    private Vector3 _endPosition;

    //The Time.time value when we started the interpolation
    private float _timeStartedLerping;

    private bool _start = false;

    [Header("")]


    public float positionSmoothingTime = 0.25f;

    [Header("OSCrx Polling Option")]

    // OSC POLLING CONTROL
    private Vector3 _oscCurValue;
    private Vector3 _oscLastValue;
    public bool OSCpollingEnable = true;
    public float OSCpollIntervalMs = 30f;  // polling interval ms for current OSC values
    private float _lastPollTime;

    private void OnValidate()
    {
        if (!_start)
            return;
        if (positionSmoothingTime <= 0)
            positionSmoothingTime = 0.0001f;
        if (OSCpollIntervalMs < 0)
            OSCpollIntervalMs = 0f;
    }


    void Start()
    {
        _oscCurValue = new Vector3(0, 0, 0);
        _oscLastValue = new Vector3(0, 0, 0);

        if (OSCaddress.Length == 0)
        {
            OSCaddress = "/" + transform.name + "/position";  // automatically create an OSC address using the object's name, making:  "/objName/position"
        }

        oscrx = zUnityOSCrx.getInstance(streamName);

        if (oscrx == null)
        {
            if (streamName == "default")
            {
                if (debug) Debug.Log(name + " " + GetType() + ".Start(): zUnityOSCrx instance for stream: " + streamName + " undefined, instantiating the default instance");
                GameObject gob = new GameObject("zUnityOSCrx.autoGen.DefaultStream");
                gob.AddComponent<zUnityOSCrx>();
                oscrx = gob.GetComponent<zUnityOSCrx>();   // creates stream with defaults, as defined in the zUnityOSCstreamInfo class
                oscrx.streamName = streamName;
            }
            else
            {
                Debug.LogError(name + " " + GetType() + " zUnityOSCrx instance for stream: " + streamName + " undefined, aborting");
                Destroy(this);
            }
        }

        _start = true;
        oscListen(true);
        OnEnable();   // this can not be called until START has set up the state
    }

    void oscListen(bool state)
    {
        if (oscrx == null) return;

        if (state)
        {
            if (oscListening != true)
            {
                // subscribe
                oscrx.OnOscMessage += OnOSCMessage;
                oscListening = true;
            }
        }
        else
        {
            if (oscListening == true)
            {
                oscrx.OnOscMessage -= OnOSCMessage;
                oscListening = false;
            }
        }
    }


    public void OnEnable()
    {

          if (!_start)
            return;

        oscListen(true);


        if (transformToMove == null)
        {
            Transform hostTransform = GetComponent<Transform>();
            if (hostTransform != null) transformToMove = hostTransform;
        }

        _xpos = transform.position.x;
        _ypos = transform.position.y;
        _zpos = transform.position.z;

    }

    void OnDisable()
    {
          if (!_start)
            return;
        oscListen(false);
    }

    public void OnOSCMessage(OSCMessage mess)
    {
        if (mess.Address == OSCaddress)
        {
            if (mess.Data.Count == 1)
                oscSetPositionY(mess);
            else if (mess.Data.Count == 2)
                oscSetPositionXZ(mess);
            else if (mess.Data.Count == 3)
                oscSetPositionXYZ(mess);
        }
        else if (mess.Address == OSCaddress + "/y") oscSetPositionY(mess);
        else if (mess.Address == OSCaddress + "/xz") oscSetPositionXZ(mess);
    }


    //We do the actual interpolation in FixedUpdate(), since we're dealing with a rigidbody
    void Update()
    {

        if (OSCpollingEnable)
        {
            if (1000 * (Time.unscaledTime - _lastPollTime) > OSCpollIntervalMs)
            {
                _lastPollTime = Time.unscaledTime;
                processData();
            }
        }


        //base.FixedUpdate ();
        if (_isLerping)
        {
            //We want percentage = 0.0 when Time.time = _timeStartedLerping
            //and percentage = 1.0 when Time.time = _timeStartedLerping + timeTakenDuringLerp
            //In other words, we want to know what percentage of "timeTakenDuringLerp" the value
            //"Time.time - _timeStartedLerping" is.
            float timeSinceStarted = Time.time - _timeStartedLerping;
            float percentageComplete = timeSinceStarted / positionSmoothingTime;

            //Perform the actual lerping.  Notice that the first two parameters will always be the same
            //throughout a single lerp-processs (ie. they won't change until we hit the space-bar again
            //to start another lerp)
            transformToMove.localPosition = Vector3.Lerp(_startPosition, _endPosition, percentageComplete);

            //When we've completed the lerp, we set _isLerping to false
            if (percentageComplete >= 1.0f)
            {
                _isLerping = false;
            }
        }
    }

        // expects one data:  y
        void oscSetPositionY(OSCMessage message)
    {
        float y = 0;

        if (message.Data.Count != 1)
        {
            Debug.LogError(transform.name + " : " + GetType() + " : " + "Wrong Value Count: " + OSCaddress + "/y val only takes one value, ignoring message");
            return;
        }

        if (message.Data[0] is float)
            y = (float)message.Data[0];
        if (message.Data[0] is int)
            y = (float)(int)message.Data[0];

        // store current value
        _oscCurValue.y = y;

        if (!OSCpollingEnable)
            processData();
    }

    // expects two data:  x z
    void oscSetPositionXZ(OSCMessage message)
    {
        float x = 0;
        float z = 0;

        if (message.Data.Count != 2)
        {
            Debug.LogWarning(transform.name + " : " + GetType() + " : " + "Wrong Value Count: " + OSCaddress + "/xz val only takes two values, ignoring message");
            return;
        }

        if (message.Data[0] is float)
            x = (float)message.Data[0];
        if (message.Data[0] is int)
            x = (float)(int)message.Data[0];

        if (message.Data[1] is float)
            z = (float)message.Data[1];
        if (message.Data[1] is int)
            z = (float)(int)message.Data[1];

        // store current value
        _oscCurValue.x = x;
        _oscCurValue.z = z;

        if (!OSCpollingEnable)
            processData();
    }


    // expects three data:  x y z
    void oscSetPositionXYZ(OSCMessage message)
    {
        float x = 0;
        float y = 0;
        float z = 0;

        if (message.Data.Count != 3)
        {
            Debug.LogWarning(transform.name + " : " + GetType() + " : " + "Wrong Value Count: " + OSCaddress + " X Y Z,ignoring message");
            return;
        }

        if (message.Data[0] is float)
            x = (float)message.Data[0];

        if (message.Data[1] is float)
            y = (float)message.Data[1];

        if (message.Data[2] is float)
            z = (float)message.Data[2];

        // store current value
        _oscCurValue.Set(x, y, z);

        if (!OSCpollingEnable)
            processData();
    }


    public void processData()
    {
        float data0, data1, data2;

        if (_oscCurValue == _oscLastValue)
            return;

        data0 = _oscCurValue.x;
        data1 = _oscCurValue.y;
        data2 = _oscCurValue.z;
        _oscLastValue = _oscCurValue;

        if (transformToMove == null) return;
        //OscMessage msg = (OscMessage)args.Packet;

        float x = transformToMove.transform.localPosition.x;
        float y = transformToMove.transform.localPosition.y;
        float z = transformToMove.transform.localPosition.z;

        //            // handle for y no matter what
        //            if (msg.Data.Count == 1)  // going to need to compute Y
        //            {
        //                if (Yenable)
        //                {
        //                    y = (Ymapping == PosMapping.direct) ? yscale * data0 : _ypos + yscale * data0;
        //                }
        //            }
        //            else if (msg.Data.Count == 2)  // going to need to compute for XY
        //            {
        //                if (Xenable)
        //                {
        //                    x = (Xmapping == PosMapping.direct) ? xscale * data0 : _xpos + xscale * data0;
        //                }
        //                if (Zenable)
        //                {
        //                    z = (Ymapping == PosMapping.direct) ? zscale * data1 : _zpos + zscale * data1;
        //                }
        //
        //            }
        //            else if (msg.Data.Count == 3)  // going to need to compute  XYZ
        //            {
        if (Xenable)
        {
            x = (Xmapping == PosMapping.direct) ? xscale * data0 : _xpos + xscale * data0;
        }
        if (Yenable)
        {
            y = (Ymapping == PosMapping.direct) ? yscale * data1 : _ypos + yscale * data1;
        }
        if (Zenable)
        {
            z = (Zmapping == PosMapping.direct) ? zscale * data2 : _zpos + zscale * data2;
        }
        //            }
        //            else
        //            {
        //                Debug.LogError("qk_OscMoveGameObject.OSCrx: expects 1, two, or three values");
        //                return;
        //            }


        targetPos = new Vector3(x, y, z);
        _isLerping = true;
        _timeStartedLerping = Time.time;

        //We set the start position to the current position, and the finish to 10 spaces in the 'forward' direction
        _startPosition = transformToMove.localPosition;
        _endPosition = targetPos;
    }
}

