﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class oscrxClientTest : MonoBehaviour {

    public string ipaddr;

    public TextMesh text;

	// Use this for initialization
	void Start () {

        if (!text) Debug.LogError(GetType()+" Start():  Text Mesh Missing");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void serverIP(string ipstr)
    {
        Debug.Log(name + " " + GetType() + ".serverIP(): " + ipaddr);
        ipaddr = ipstr;
        text.text = "my ip: "+ipstr;
    }

}
