﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityOSC;




// THIS CLASS NEEDS INSTANCE SUBSCRIPTION ON STREAM NAME
public class zUnityOSCrx : MonoBehaviour
{

    public delegate void OnOscMessageEventHandler(OSCMessage m);
    public event OnOscMessageEventHandler OnOscMessage;

    public static List<zUnityOSCrx> instances = new List<zUnityOSCrx>();

    public bool oscMute = false;
    private OSCReciever reciever = null;
    public string streamName = "default";
    public int port = 9999;
    //private zUnityOSCstreamInfo streamInfoInstance;
    public bool debug = false;
    private bool oscListening = false;
    //private OSCServer server;


    private void Awake()
    {
        if (streamName == "")
        {
            streamName = "default";
            port = 9999;
        }

        if (getInstance(streamName) != null)
        {
            Debug.LogWarning(name + " " + GetType() + ".Awake():  instance named: " + streamName + " already exists, ignoring instantation");
            //Destroy(this);
        }

    }

    public static zUnityOSCrx getInstance(string sName)
    {
        foreach (zUnityOSCrx instance in instances)
        {
            if (instance.streamName == sName)
            {
                return (instance);
            }
        }
        return (null);
    }

    // Use this for initialization
    void Start()
    {
        if (debug) Debug.Log(name + " " + GetType() + ".Start(): setting OSCrx to port: " + port);
        reciever = new OSCReciever();
        reciever.Open(port);
        oscListen(true);

    }


    // Update is called once per frame
    void Update()
    {

        if (reciever.hasWaitingMessages())
        {
            OSCMessage msg = reciever.getNextMessage();
            if (debug) Debug.Log(name + " " + GetType() + ".Update() port:" + port+" stream: "+streamName+string.Format(" message received: {0} {1}", msg.Address, DataToString(msg.Data)));
            if (OnOscMessage != null) OnOscMessage(msg);
        }
    }

    void oscListen(bool state)
    {
        if (reciever == null) return;   // don't do anything until object has is initialized

        if (state)
        {
            if (oscListening != true)
            {
                reciever.Open(port);
                // subscribe
                //reciever.MessageReceivedEvent += OnMessagetReceived;
                oscListening = true;
            }
        }
        else
        {
            if (oscListening == true)
            {
                // unsubscribe
                //reciever.MessageReceivedEvent -= OnMessagetReceived;
                reciever.Close();
                oscListening = false;
            }
        }
    }


    private void OnEnable()
    {
        oscListen(true);
        instances.Add(this);
    }


    private void OnDisable()
    {
        oscListen(false);
        instances.Remove(this);
    }

    private void OnDestroy()
    {
        oscListen(false);
        //reciever.Close();
    }

    private string DataToString(List<object> data)
    {


        //foreach (object obj in data)
        //{
        //    string strval = obj.ToString();

        //    int n;
        //    float f;

        //    bool isNumericInt = int.TryParse(strval, out n);
        //    bool isNumericFloat = float.TryParse(strval, out f);

        //    if (isNumericInt)
        //        Debug.Log("DataToString:  Value: " + n + " is an int");
        //    else if (isNumericFloat)
        //        Debug.Log("DataToString:  Value: " + f + " is a float");


        //    else
        //        Debug.Log("DataToString:  Value: " + strval + "is not a number");
        //}


        string buffer = "";

        for (int i = 0; i < data.Count; i++)
        {
            buffer += data[i].ToString() + " ";
        }

        buffer += "\n";

        return buffer;
    }

}
