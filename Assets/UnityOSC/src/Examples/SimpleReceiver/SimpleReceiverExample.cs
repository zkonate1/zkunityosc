﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityOSC;

public class SimpleReceiverExample : MonoBehaviour {
	
	private OSCReciever reciever;

	public int port = 8338;
	
	// Use this for initialization
	void Start () {
		reciever = new OSCReciever();
		reciever.Open(port);

    }
	
	// Update is called once per frame
	void Update () {
    
		if(reciever.hasWaitingMessages()){
			OSCMessage msg = reciever.getNextMessage();
			Debug.Log("port: "+port+string.Format("message received: {0} {1}", msg.Address, DataToString(msg.Data)));
		}
	}
	
	private string DataToString(List<object> data)
	{


        foreach(object obj in data)
        {
            string strval = obj.ToString();

            int n;
            float f;

            bool isNumericInt = int.TryParse(strval,  out n);
            bool isNumericFloat = float.TryParse(strval, out f);

            if (isNumericInt)
                Debug.Log("DataToString:  Value: " + n + " is an int");
            else if (isNumericFloat)
                Debug.Log("DataToString:  Value: " + f + " is a float");


            else
                Debug.Log("DataToString:  Value: " + strval + "is not a number");
        }


        string buffer = "";
		
		for(int i = 0; i < data.Count; i++)
		{
			buffer += data[i].ToString() + " ";
		}
		
		buffer += "\n";
		
		return buffer;
	}
}
